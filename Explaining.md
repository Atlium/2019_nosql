Create util class and in Main class create array of employees in employees.json
After installing mongo i edit config for allow replication and call it FirstReplica(resources/mongoCfg.png)
Creating instances in createInstances.bat, after launching launchedInstances.png
After that i create configureReplicaSet.js for configuring replicaSet, instances with port 27001(2) are secondary, 27003 arbitter, 27004 delayed and 27005 primary
Launch configureReplicaSet.bat and result of this on configuringReplicaSetAndLoadJsonToPrimary.png, also i import employees.json to primary replica
Status of replica set on replicaSetStatus.png
After creating replica set, in the same script, i export employees collection from primary and delayed replicas to different files(primaryAfterLoadingJson.png and delayedAfterLoadingJson.png)
And after 10 sec(this delay in configureReplicaSet.js in property slaveDelay) i manually by console export this collection to another file from delayedReplica delayedAfterDelay.png