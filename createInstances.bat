cd "C:\Program Files\MongoDB\Server\4.2\bin\"
start cmd.exe /k mongod --dbpath "C:\Atlium\MongoDB\data\db1" --port 27001 --replSet FirstReplica --logpath "C:\Atlium\MongoDB\data\db1\db1.txt"
start cmd.exe /k mongod --dbpath "C:\Atlium\MongoDB\data\db2" --port 27002 --replSet FirstReplica --logpath "C:\Atlium\MongoDB\data\db2\db2.txt"
start cmd.exe /k mongod --dbpath "C:\Atlium\MongoDB\data\dbArbitter" --port 27003 --replSet FirstReplica --logpath "C:\Atlium\MongoDB\data\dbArbitter\dbArbitter.txt"
start cmd.exe /k mongod --dbpath "C:\Atlium\MongoDB\data\dbDelayed" --port 27004 --replSet FirstReplica --logpath "C:\Atlium\MongoDB\data\dbDelayed\dbDelayed.txt"
start cmd.exe /k mongod --dbpath "C:\Atlium\MongoDB\data\dbPrimary" --port 27005 --replSet FirstReplica --logpath "C:\Atlium\MongoDB\data\dbPrimary\dbPrimary.txt"
exit
