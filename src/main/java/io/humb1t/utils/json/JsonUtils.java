package io.humb1t.utils.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.Serializable;

public class JsonUtils {
    private static final Logger LOGGER = Logger.getLogger(JsonUtils.class);

    private JsonUtils() {
        //util
    }

    public static String parseToJson(Serializable s) {
        try {
            return new ObjectMapper().writeValueAsString(s);
        } catch (Exception e) {
            LOGGER.error("Error while parse object to string", e);
        }
        return null;
    }
}
