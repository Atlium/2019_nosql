package io.humb1t.utils.date;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class DateUtils {
    private static final String TIMESTAMP_FORMAT = "dd.MM.yyyy HH:mm:ss";

    private DateUtils() {
        //util
    }

    public static Date getSqlDate(java.util.Date date) {
        if (date == null) {
            return null;
        }
        return new Date(date.getTime());
    }

    public static java.util.Date getUtilDate(Date date) {
        if (date == null) {
            return null;
        }
        return new Date(date.getTime());
    }

    public static String formatToTimestamp(java.util.Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(TIMESTAMP_FORMAT).format(date);
    }
}
