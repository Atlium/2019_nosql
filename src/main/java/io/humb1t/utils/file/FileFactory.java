package io.humb1t.utils.file;

import java.io.File;
import java.net.URL;

public class FileFactory {

    private static final String ERROR_FILE_NAME_EMPTY = "file name can't be empty";
    private static final String ERROR_FILE_NOT_FOUND = "file is not found!";

    public File getFile(FileSource fileSource, String name) {
        switch (fileSource) {
            case RESOURCE:
                return getFileFromResources(name);
            case FILE_SYSTEM:
                return getFileFromSystem(name);
        }
        return getFileFromSystem(name);
    }


    private File getFileFromSystem(String fileName) {
        if (fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException(ERROR_FILE_NAME_EMPTY);
        }
        return new File(fileName);
    }

    private File getFileFromResources(String fileName) {
        if (fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException(ERROR_FILE_NAME_EMPTY);
        }
        ClassLoader classLoader = getClass().getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException(ERROR_FILE_NOT_FOUND);
        } else {
            return new File(resource.getFile());
        }
    }
}
