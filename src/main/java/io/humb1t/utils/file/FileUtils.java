package io.humb1t.utils.file;

import java.io.*;

public class FileUtils {
    private FileUtils() {
        //util class
    }

    public static String getTextFromFile(File file) {
        if (file == null) {
            throw new IllegalArgumentException("File can't be a null");
        }
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String nextLine;
            while ((nextLine = reader.readLine()) != null) {
                if (builder.length() != 0) {
                    builder.append("\n");
                }
                builder.append(nextLine);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + file.toString());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return builder.toString();
    }
}
