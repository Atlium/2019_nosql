package io.humb1t.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.humb1t.utils.date.DateUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Employee implements Serializable {
    private static final long serialVersionUID = -910931630508850481L;

    private Long id;
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    private Date employmentDate;
    private Man man;
    private Department department;
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    private Date dismissDate;

    public Employee() {
    }

    public Employee(Long id, Date employmentDate, Man man, Department department, Date dismissDate) {
        this.id = id;
        this.employmentDate = employmentDate;
        this.man = man;
        this.department = department;
        this.dismissDate = dismissDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(Date employmentDate) {
        this.employmentDate = employmentDate;
    }

    public Man getMan() {
        return man;
    }

    public void setMan(Man man) {
        this.man = man;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Date getDismissDate() {
        return dismissDate;
    }

    public void setDismissDate(Date dismissDate) {
        this.dismissDate = dismissDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return Objects.equals(getId(), employee.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", employmentDate=" + DateUtils.formatToTimestamp(employmentDate) +
                ", man=" + man +
                ", department=" + department +
                ", dismissDate=" + DateUtils.formatToTimestamp(dismissDate) +
                '}';
    }
}
