package io.humb1t.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.humb1t.utils.date.DateUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Man implements Serializable {
    private static final long serialVersionUID = -7318489020137516160L;

    private Long id;
    private String firstName;
    private String secondName;
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    private Date birthDate;

    public Man() {
    }

    public Man(Long id, String firstName, String secondName, Date birthDate) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthDate = birthDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Man)) return false;
        Man man = (Man) o;
        return Objects.equals(getId(), man.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Man{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", birthDate=" + DateUtils.formatToTimestamp(birthDate) +
                '}';
    }
}
