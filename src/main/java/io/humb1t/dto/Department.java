package io.humb1t.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.humb1t.utils.date.DateUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Department implements Serializable {
    private static final long serialVersionUID = -5675685714704378136L;

    private Long id;
    private String deptName;
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    private Date foundationDate;

    public Department() {
    }

    public Department(Long id, String deptName, Date foundationDate) {
        this.id = id;
        this.deptName = deptName;
        this.foundationDate = foundationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Date getFoundationDate() {
        return foundationDate;
    }

    public void setFoundationDate(Date foundationDate) {
        this.foundationDate = foundationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", deptName='" + deptName + '\'' +
                ", foundationDate=" + DateUtils.formatToTimestamp(foundationDate) +
                '}';
    }
}
