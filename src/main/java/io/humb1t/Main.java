package io.humb1t;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.humb1t.dto.Department;
import io.humb1t.dto.Employee;
import io.humb1t.dto.Man;
import io.humb1t.utils.file.FileFactory;
import io.humb1t.utils.file.FileSource;
import io.humb1t.utils.json.JsonUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class);

    private static final String JSON_NAME = "employees.json";
    private static final FileSource JSON_SOURCE = FileSource.FILE_SYSTEM;

    public static void main(String[] args) {
        createJson();
    }

    private static void createJson() {
        FileFactory fileFactory = new FileFactory();
        File file = fileFactory.getFile(JSON_SOURCE, JSON_NAME);
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            ObjectMapper objectMapper = new ObjectMapper();
            List<Employee> employees = getEmployees();
            objectMapper.writeValue(fileOutputStream, employees);
            List<String> printedEmployees = employees
                    .stream()
                    .map(JsonUtils::parseToJson)
                    .collect(Collectors.toList());
            printedEmployees.forEach(LOGGER::info);
        } catch (Exception e) {
            LOGGER.error("Error while translate entities to JSON", e);
        }
    }

    private static List<Employee> getEmployees() {
        int size = 10;
        Calendar c = Calendar.getInstance();
        Date startDate = new Date();
        c.setTime(startDate);
        c.add(Calendar.YEAR, -30);
        List<Man> mams = LongStream.range(0L, size).mapToObj(i -> {
            c.add(Calendar.YEAR, 1);
            return new Man(i, "ManName" + i, "ManLastName" + i, c.getTime());
        }).collect(Collectors.toList());
        c.setTime(startDate);
        c.add(Calendar.YEAR, -40);
        List<Department> departments = LongStream.range(0L, size).mapToObj(i -> {
            c.add(Calendar.YEAR, 1);
            return new Department(i, "DeptName" + i, c.getTime());

        }).collect(Collectors.toList());
        return IntStream.range(0, size)
                .mapToObj(i -> new Employee((long) i, new Date(), mams.get(i), departments.get(i), null))
                .collect(Collectors.toList());
    }
}
