rs.initiate({
    "_id": "FirstReplica",
    members: [
        {"_id": 0, host: "127.0.0.1:27001"},
        {"_id": 1, host: "127.0.0.1:27002"},
        {"_id": 2, host: "127.0.0.1:27003", arbiterOnly: true},
        {"_id": 3, host: "127.0.0.1:27004", priority: 0, slaveDelay: 10, hidden: true},
        {"_id": 4, host: "127.0.0.1:27005", priority: 3}
    ]
})